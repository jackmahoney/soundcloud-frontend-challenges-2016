#Soundcloud front-end challenges submission

Each folder contains a challenge submission with a SUBMISSION.md documenting the project.
README.md is the original challenge task readme.

###Feedback
Fun challenges. But I think the animation challenge is a bit vague or unrealistic. Usually developers and designers would workshop a transition like this. Guessing what transition I think is appropriate for the user isn't really the developer's job, but I did what I thought was best. I think my real CSS/HTML skills are better demonstrated in a different challenge. Still, happy to have done it.