/**
 * Load-images XHR/BLOG-based optimization
 */
SC.loadImages = (function () {

    /**
     * @see original
     */
    function loadImages(urls) {
        log('Starting to load ' + urls.length + ' images');

        var allDone = $.Deferred(),
            startTime = Date.now(),
            deferreds;

        deferreds = urls.map(loadImage);

        $.when.apply($, deferreds).then(function () {
            log('Loading ' + urls.length + ' took ' + (Date.now() - startTime) + 'ms');

            // pick out the Image elements from the resolution values of each deferred, and resolve the overall deferred with
            // these images.
            allDone.resolve(_.compact(_.pluck(arguments, 0)));
        });

        allDone.fail(function () {

            /* OPTIMIZATION */
            //when we cancel a bulk load cycle let's reject all the individual loads
            _.each(deferreds, function (v, i) {
                v.reject();
            });

            log('Loading ' + urls.length + ' images cancelled');
        });

        return allDone;
    }

    /**
     * Create a cross domain request object. In reality we'd use a library here
     * @param method
     * @param url
     * @returns {XMLHttpRequest}
     */
    /* OPTIMIZATION */
    function createCORSRequest(method, url) {

        var xhr = new XMLHttpRequest();

        if ("withCredentials" in xhr) {
            xhr.open(method, url, true);
        } else if (typeof XDomainRequest != "undefined") {
            xhr = new XDomainRequest();
            xhr.open(method, url);
        } else {
            //we won't do proper error handling for the demo
            throw new Error('CORS not supported');
        }
        return xhr;

    }

    /**
     * Given a single URL, return a deferred which is resolved once the image is loaded or its loading has failed.
     *
     * For our purposes, a failed load is okay. If the load is successful, the deferred is resolved with an Image element.
     *
     * @param {String} url
     * @return {Deferred}
     */
    /* OPTIMIZATION */
    //The idea here is to use a CORS request to load an image. Then we convert it into a BLOB and then into an ObjectURL.
    //The ObjectURL is a special browser created URL that can be assigned to the image src. The advantage of this technique
    //is that we can cancel the request easily when the deferred is cancelled.
    function loadImage(url) {

        var deferred = $.Deferred();

        //create a cross domain request
        var xhr = createCORSRequest('GET', url);
        var urlCreator = window.URL || window.webkitURL;
        var imageUrl;

        xhr.responseType = "arraybuffer";

        //on request load
        xhr.onload = function(e) {

            // convert the image data into a blob url
            var arrayBufferView = new Uint8Array(this.response);
            var blob = new Blob([arrayBufferView], { type: "image/jpeg" });

            imageUrl = urlCreator.createObjectURL(blob);

            //create an image
            var img = new Image();
            img.onload = deferred.resolve.bind(deferred, img);
            img.onerror = deferred.resolve; // no img means it failed, but that's okay, we just won't draw it.
            img.src = imageUrl;
        };

        //fire away, boys!
        xhr.send();

        //if the image load is cancelled abort the xhr request
        deferred.fail(function(){
            //abort the request
            xhr.abort();
            //clean up any object url
            urlCreator.revokeObjectURL(imageUrl);
        });

        return deferred;
    }

    // for some debugging messages
    function log (str) {
        $('#log').append($('<li></li>').text(str));
    }

    return loadImages;
}());
