#Queue
Queue optimization submission.

##Structure
Two different submissions: `load-images-src.js` and `load-images-xhr.js`. Edit `index.html` and uncomment one of the
scripts to test either. I've also included the original.

I only modified a few lines in the original functions. I marked these areas with `/* OPTIMIZATION */` and left the rest.

##Concepts
The XHR submission is definitely the best. The idea is we move loading images from the image object to a request object.
I use a CORS XHR request object to fetch an image. Then I convert it into a BLOB and create an ObjectURL for it.
I assign this ObjectURL to an image object and resolve the deferred. The advantage here is that the XHR request
can be aborted at any time (ie: when the deferred is cancelled). This mean switching from loading 2000 to loading 10
is fast. Note this method *appears* to "double" the request in the inspector. It's just because the image object
is using an ObjectURL and this gets counted as a request (but it isn't really).

The second submission is an image SRC hack. It's performs poorly but I included it anyway.
Here I simply set the image.src to an empty string when the deferred is cancelled.
But, it's not as smooth as the XHR optimization.
