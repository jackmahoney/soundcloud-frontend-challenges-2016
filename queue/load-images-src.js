/**
 * Load-images image.src optimization
 */
SC.loadImages = (function () {

    /**
     * @see original
     */
    function loadImages (urls) {
        log('Starting to load ' + urls.length + ' images');

        var allDone = $.Deferred(),
            startTime = Date.now(),
            deferreds;

        deferreds = urls.map(loadImage);

        $.when.apply($, deferreds).then(function () {
            log('Loading ' + urls.length + ' took ' + (Date.now() - startTime) + 'ms');
            allDone.resolve(_.compact(_.pluck(arguments, 0)));
        });

        allDone.fail(function () {

            /* OPTIMIZATION */
            _.each(deferreds, function(v, i){
                v.reject();
            });

            log('Loading ' + urls.length + ' images cancelled');
        });

        return allDone;
    }

    /**
     * Given a single URL, return a deferred which is resolved once the image is loaded or its loading has failed.
     *
     * For our purposes, a failed load is okay. If the load is successful, the deferred is resolved with an Image element.
     *
     * @param {String} url
     * @return {Deferred}
     */
    /* OPTIMIZATION */
    function loadImage(url) {

        var deferred = $.Deferred();
        var img = new Image();

        img.onload = deferred.resolve.bind(deferred, img);
        img.onerror = deferred.resolve; // no img means it failed, but that's okay, we just won't draw it.

        //start loading the image
        img.src = url;

        deferred.fail(function(){
            //if deferred is cancelled then cancel the request by setting image src to empty string
            img.src = '';
        });

        return deferred;
    }

    // for some debugging messages
    function log (str) {
        $('#log').append($('<li></li>').text(str));
    }

    return loadImages;
}());
