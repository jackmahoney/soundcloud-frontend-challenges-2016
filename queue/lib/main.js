// Code in here shouldn't need to be changed, but might be useful to understand the flow of the demonstration

(function () {
  var manyDeferred,
      fewDeferred,
      step = 0,
      canvas = document.getElementById('waveform'),
      ctx = canvas.getContext('2d');


  // loads 2000 images
  function loadAndShowMany() {
    manyDeferred = SC.loadImages(SC.avatarsMany).done(showImages);
  }

  // loads 10 images
  function loadAndShowFew() {
    ctx.clearRect(0, 0, 800, 60);

    // stop loading the previous avatars
    manyDeferred.reject();
    manyDeferred = null;

    fewDeferred = SC.loadImages(SC.avatarsFew).done(showImages);
  }

  // draw the given array of images onto the waveform
  function showImages (images) {
    images.forEach(function (img) {
      ctx.drawImage(img, Math.random() * 780, 40);
    });
  }

  // some really dodgy program flow. Nothing to see here.
  function reset() {
    step = 0;
    ctx.clearRect(0, 0, 800, 60);
    fewDeferred.reject();
    fewDeferred = null;
  }

  $('#theButton').on('click', function () {
    switch (step++) {
      case 0:   // start loading a lot of images
        loadAndShowMany();
        this.innerHTML = 'Load only a few';
        break;
      case 1:
        loadAndShowFew();
        this.innerHTML = 'Reset';
        break;
      case 2:
        reset();
        this.innerHTML = 'Start';
    }
  });
}());
