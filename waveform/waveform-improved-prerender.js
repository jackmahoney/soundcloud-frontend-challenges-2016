/**
 * This improvement to waveform.js uses a pre-rendering technique.
 * In my mind it is less optimal that waveform-improved-loop.js but the concept is nonetheless interesting.
 * Basically, we render the full waveform twice at the beginning, on offscreen canvases. Once as background color,
 * the other as active color. Then in the loop we simply layer those images on top of each other. So there is an upfront
 * cost but very fast performance after that.
 */
(function () {

    function Waveform (options) {
        this.sound = options.sound;
        this.canvas = options.canvas;
        this.canvas.addEventListener('click', this.handleClick.bind(this), true);

        //PERF: we'll use an offscreen canvas for prerendering to avoid loads of onscreen repaints
        this.prerenderCanvasBackground = document.createElement('canvas');
        this.prerenderCanvasActive = document.createElement('canvas');

        //call canvas setup once in beginning and then on resize
        this.setCanvasProperties();
        $(window).resize(this.handleResize.bind(this));
    }

    SC.Waveform = Waveform;

    _.extend(Waveform.prototype, SC.Events, {

        //to ensure first paint always succeeds
        currentTime: -1,
        colorActive: '#f60',
        colorBg: '#333',

        /**
         * This resets all the canvases and contexts (both offscreen and onscreen)
         * It also prerenderes the waveforms for use in the loop
         */
        setCanvasProperties: function(){
            //PERF: get canvas context and dimensions once
            this.ctx = this.canvas.getContext('2d');
            this.canvasH = this.canvas.offsetHeight;
            this.canvasW = this.canvas.offsetWidth;

            //reset the background prerender canvas
            this.prerenderCanvasBackground.width = this.canvasW;
            this.prerenderCanvasBackground.height = this.canvasH;
            this.prerenderBackgroundCtx = this.prerenderCanvasBackground.getContext('2d');

            //reset the active prerender canvas
            this.prerenderCanvasActive.width = this.canvasW;
            this.prerenderCanvasActive.height = this.canvasH;
            this.prerenderActiveCtx = this.prerenderCanvasActive.getContext('2d');

            this.renderWaveform(this.prerenderActiveCtx, this.colorActive);
            this.renderWaveform(this.prerenderBackgroundCtx, this.colorBg);
        },

        /**
         * Called on resize events. Forces canvas resets and redraws
         */
        handleResize: function(){
            //recalc canvas properties on resize
            this.setCanvasProperties();
            //redraw to new dimensions
            this.redraw();
        },

        /**
         * Draw the canvas the first time. This is called once only, and before any calls to `update()`.
         */
        render: function () {
            //PERF: start the update loop. we won't use the timeUpdated event system, we'll use requestAnimation
            //I would update the sound.js event to trigger play and stop events if it was in scope
            this.update();
        },

        /**
         * Handle click event, find position on waveform and seek to time on sound
         * @param e
         */
        handleClick: function(e){
            //calculate x position relative to canvas
            var relativeX = e.pageX - this.canvas.getBoundingClientRect().left;

            //what proportion of track is covered
            var proportion = relativeX / this.canvas.offsetWidth;

            //find sought time
            var soughtTime = proportion * this.sound.duration;

            //BONUS: seek to the sought time
            this.sound.seek(soughtTime);
        },

        /**
         * Update loop. Determine whether we should redraw
         */
        update: function () {

            //PERF: only redraw if time has changed
            if(this.sound.currentTime !== this.currentTime){

                this.redraw();
            }

            //PERF: use request animation frame instead of forcing updates with event system
            window.requestAnimationFrame(this.update.bind(this));
        },

        /**
         * Draw the waveform to the canvas
         */
        redraw: function(){

            this.currentTime = this.sound.currentTime;

            //how far (pixels) through the song are we?
            var progress = this.sound.currentTime / this.sound.duration * this.canvasW;

            this.ctx.clearRect(0, 0, this.canvasW, this.canvasH);

            //draw the background canvas
            this.ctx.drawImage(this.prerenderCanvasBackground, 0, 0);

            //draw a section of the active canvas on top of the background
            this.ctx.drawImage(this.prerenderCanvasActive, 0, 0, progress, this.canvasH, 0, 0, progress, this.canvasH);

        },

        /**
         * Render the waveform to a given context with a given fillstyle
         * @param context
         * @param fillStyle
         */
        renderWaveform: function(context, fillStyle){

            context.fillStyle = fillStyle;

            //get wave data and canvas context
            var data = this.sound.waveformData;

            //clear the canvas
            context.clearRect(0, 0, this.canvasW, this.canvasH);

            //iterate canvas cells
            for (var x = 0; x < this.canvasW; x++) {

                var sampleInd = (x * data.width / this.canvasW) | 0;
                var value = (this.canvasH * data.samples[sampleInd] / data.height / 2) | 0;

                context.fillRect(x, value, 1, this.canvasH - (2 * value));
            }

        }
    });

}());
