#Waveform
Performance fixes for waveform.js

##Structure
I have submitted two solutions for the waveform optimization problem. `waveform-improved-loop.js` and
`waveform-improved-prerender.js`. Two load either of these uncomment the desired script in `index.html`.

Each script is commented and I also cover the decisions behind each here in the solutions section. I use npm only to
lint my code using your jshintrc.

##Problems
The original waveform.js has a number of problems.

##Solutions

####Improved loop
Idea behind improved-loop was to optimize the existing technique without fundamentally altering it.
This included reducing jquery and DOM interaction, caching values outside of the tight loop, using
requestAnimation instead of events and rendering off-canvas. I also Reduce paints by painting one rectangle
per x coordinate and filling it, instead of drawing every single pixel. This saves tens of thousands of paints.
Overall, I saw a very large improvement in performance.

####Improved prerender
The second idea, improved-prerender, was to approach the problem from a different angle. Here I rendered
two versions of the waveform upfront (using the optimized loop from the first approach), one active, one
background-colored, and simple layer them over each other during the draw loop. This means the calculations and
expensive loops are only done at the beginning or on resize. Initial performance blip at the start but after that
very low cost draw cycles.

##Results
Results from a **6 second** timeline recording midplay active wave in Chrome.
You can also view these as screenshot images in the reports folder.


    | Measure        | original js | improved-loop | improved-prerender
    |----------------|-------------|---------------|---------------------
    | FPS            | 14          | 59/60         | 59/60
    | Time scripting | 5.12s       | 0.781s        | 0.194s
    | Time rendering | 2.70ms      | 15.91ms       | 15.06ms
    | Time painting  | 28.37ms     | 114.66ms      | 145ms
    | Time other     | 858.77ms    | 306.29ms      | 293.35ms
    | Time idle      | 4.93ms      | 4750ms        | 5380ms

