/**
 * A view of a waveform.
 * @revision Jack Mahoney
 */
(function () {

    function Waveform (options) {
        this.sound = options.sound;
        this.canvas = options.canvas;
        this.canvas.addEventListener('click', this.handleClick.bind(this), true);

        //PERF: we'll use an offscreen canvas for prerendering to avoid loads of onscreen repaints
        this.prerenderCanvas = document.createElement('canvas');

        this.setCanvasProperties();
        $(window).resize(this.handleResize.bind(this));
    }

    SC.Waveform = Waveform;

    _.extend(Waveform.prototype, SC.Events, {

        //to ensure first paint always succeeds
        currentTime: -1,

        colorActive: '#f60',
        colorBg: '#333',

        setCanvasProperties: function(){
            //PERF: get canvas context and dimensions once
            this.ctx = this.canvas.getContext('2d');
            this.canvasH = this.canvas.offsetHeight;
            this.canvasW = this.canvas.offsetWidth;

            //reset the prerender canvas
            this.prerenderCanvas.width = this.canvasW;
            this.prerenderCanvas.height = this.canvasH;
            this.prerenderCtx = this.prerenderCanvas.getContext('2d');
        },

        handleResize: function(){
            //recalc canvas properties on resize
            this.setCanvasProperties();
            //redraw to new dimensions
            this.redraw();
        },

        /**
         * Draw the canvas the first time. This is called once only, and before any calls to `update()`.
         */
        render: function () {
            //PERF: start the update loop. we won't use the timeUpdated event system, we'll use requestAnimation
            //I would update the sound.js event to trigger play and stop events if it was in scope
            this.update();
        },

        /**
         * Handle click event, find position on waveform and seek to time on sound
         * @param e
         */
        handleClick: function(e){
            //calculate x position relative to canvas
            var relativeX = e.pageX - this.canvas.getBoundingClientRect().left;

            //what proportion of track is covered
            var proportion = relativeX / this.canvas.offsetWidth;

            //find sought time
            var soughtTime = proportion * this.sound.duration;

            //BONUS: seek to the sought time
            this.sound.seek(soughtTime);
        },

        /**
         * Update loop. Determine whether we should redraw
         */
        update: function () {

            //PERF: only redraw if time has changed
            if(this.sound.currentTime !== this.currentTime){

                this.redraw();
            }

            //PERF: use request animation frame instead of forcing updates with event system
            window.requestAnimationFrame(this.update.bind(this));
        },

        /**
         * Draw the waveform to the canvas
         */
        redraw: function(){

            //store time we have painted as separate record from the sound time
            this.currentTime = this.sound.currentTime;

            //PERF:reduce fillStyle changes, only set it twice
            var inBackgroundFill = this.sound.currentTime === 0 ;
            this.prerenderCtx.fillStyle = inBackgroundFill ? this.colorBg : this.colorActive;

            //get wave data and canvas context
            var data = this.sound.waveformData;

            //only clear whats necessary
            this.prerenderCtx.clearRect(0, 0, this.canvasW, this.canvasH);

            //iterate canvas cells
            for (var x = 0; x < this.canvasW; x++) {

                //PERF: Math.floor to |0 in tight loop
                var sampleInd = (x * data.width / this.canvasW) | 0;
                var value = (this.canvasH * data.samples[sampleInd] / data.height / 2) | 0;

                //PERF: set fill style once we cross into unplayed rectangles
                if(!inBackgroundFill && x > this.sound.currentTime / this.sound.duration * this.canvasW){
                    inBackgroundFill = true;
                    this.prerenderCtx.fillStyle = this.colorBg;
                }

                //PERF: only draw one rect per x coordinate to increase FPS
                this.prerenderCtx.fillRect(x, value, 1, this.canvasH - (2 * value));
            }

            //PERF: now render to our real canvas
            this.ctx.clearRect(0, 0, this.canvasW, this.canvasH);
            this.ctx.drawImage(this.prerenderCanvas, 0, 0);
        }
    });

}());
