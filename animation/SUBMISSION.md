#Animation
A mock artist embed with transitions.

##Caveats
Only tested in latest Chrome and Firefox on OSX.
The waveform is an image. If it the embed was real, this would be rendered on a canvas and I'd have more control over
size and time-unit placements. I think my real CSS/HTML understanding and skills go far beyond this example (I can
demonstrate that with real work).

##Process
I used CSS and no preprocessor (keeping with instructions). For real projects I would always use SASS or LESS.
I could have made the animations complicated but I wanted subtle transitions that gave a sense of movement but didn't
 overwhelm. I gave the waveform a jiggle animation to imitate the loading of the canvas waveform.

Open `index.html` in a browser to test.
