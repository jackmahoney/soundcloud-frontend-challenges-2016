/**
 * Embed javascripts
 */
(function(){

    //add click listeners that trigger a class on the container
    var classActive = 'embed-active';
    var container = document.getElementById('container');
    var button = document.getElementById('play-control');
    var artistLink = document.getElementById('artist-link');

    button.addEventListener('click', function(){
        container.classList.add(classActive);
    });

    artistLink.addEventListener('click', function(){
        container.classList.remove(classActive);
    });

})();