var select = require('./select.js');
var fs = require('fs');
/**
 * Simple cli for using select.js
 */
//check cli args
if(process.argv.length < 3){
    var str = 'Error: Must pass at least 1 argument.' +
        '\n\n\t- usage: select-cli.js <filename.json> <json options>' +
        '\n\n\t- example: select-cli.js sample.json \'{"id": 4, "auto": false}\'' +
        '\n\n';

    console.error(str);
    process.exit(0);
}

//read json file
var filePath = process.argv[2];
var data = {};
var options = {};

try{
    data = JSON.parse(fs.readFileSync(filePath, 'utf8'));
}
catch(e){
    console.log('Error: Reading file failed. ' + e);
    process.exit(0);
}

//get options
if(process.argv[3]){
    try{
        options = JSON.parse(process.argv[3]);
    }
    catch(e){
        console.log('Error: Reading json options. ' + e);
        process.exit(0);
    }
}

var results = select(data, options);
var header = results.length + ' results from ' + data.length + ' entries in ' + filePath;
console.log(results);
console.log(header);
