/**
 * @param dataSet {data}
 * @param options optional
 */
function select(_dataSet, _options){

    //allow for optional arguments
    var dataSet = _dataSet || [],
        options = _options || {},
        results = [];

    //if merge required then perform this first
    //we could get better performance merging after filtering but
    //api dictates initial merge
    if(options.merge){
        dataSet = mergeDataSet(dataSet);
    }

    //filter out those that don't meet criteria
    for (var i = 0; i < dataSet.length; i++) {

        var data = dataSet[i],
            valid = true;

        //filter id
        if(options.id && data.id !== options.id){
            valid = false;
        }

        //filter on auto
        if(typeof options.auto !== 'undefined' && options.auto !== data.auto){
            valid = false;
        }

        //filter min play time
        if(options.minPlayTime && data.playTime < options.minPlayTime){
            valid = false;
        }

        //if still valid then store result
        if(valid){
            results.push(data);
        }
    }

    return results;

}

/**
 * Merge a data set based on id.
 * Inserts merged objects in the last position they are found.
 * @private
 * @param dataSet {array}
 * @returns {Array}
 */
function mergeDataSet(dataSet){
    //empty array that we will fill with newly merged entries
    var mergedDataSet = [];

    //iterate dataSet in reverse so that merged objects
    //are added at the "final" position once we reverse it
    for (var i = dataSet.length - 1; i >= 0; i--) {

        var data = dataSet[i],
            valid = true;

        //find the duplicate id and merge if necessary
        for (var j = 0; j < mergedDataSet.length; j++) {

            var candidate = mergedDataSet[j];

            if(candidate.id == data.id){
                //ensure we don't add this again
                valid = false;

                //merge data with previous instance
                mergedDataSet[j] = mergeObjects(candidate, data);
            }

        }

        if(valid){
            mergedDataSet.push(data);
        }

    }

    //reverse the merged data set to preserve order for unmerged objects
    //that matches original
    return mergedDataSet.reverse();
}

/**
 * Merge two data objects
 * @private
 * @param a {data}
 * @param b {data}
 * @returns {data}
 */
function mergeObjects(a, b){

    //create a new object to transfer properties on to
    var c = {};

    //preserve id
    c.id = a.id;

    //combine playTimes
    c.playTime = a.playTime + b.playTime;

    //favour false autos
    if(a.auto === true){
        c.auto = b.auto;
    }
    else{
        c.auto = false;
    }

    return c;
}

module.exports = select;