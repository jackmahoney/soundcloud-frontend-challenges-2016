var expect = require('chai').expect;
var select = require('../src/select.js');

var data = [
    { id: 8, playTime:  500, auto: false },
    { id: 7, playTime: 1500, auto: true  },
    { id: 1, playTime:  100, auto: true  },
    { id: 7, playTime: 1000, auto: false },
    { id: 7, playTime: 2000, auto: false },
    { id: 2, playTime: 2000, auto: true  },
    { id: 2, playTime: 2000, auto: true  }
];

describe("Aggregate", function(){

    describe("#select()", function(){

        it("should return handle no arguments or empty arrays", function(){
            expect(select()).deep.eql([]);
            expect(select([])).deep.eql([]);
        });

        it("should return full data set if no options specified", function(){
            var results = select(data);
            expect(results).deep.eql(data);
        });

        it("should, when specified, merge items based on id by summing playTime and and setting auto to false if any auto.false instances found", function(){
            var results = select(data, { merge: true });
            var expectation = [
                { id: 8, playTime:  500, auto: false },
                { id: 1, playTime:  100, auto: true  },
                { id: 7, playTime: 4500, auto: false },
                { id: 2, playTime: 4000, auto: true  }
            ];
            expect(results).deep.eql(expectation);
        });

        it("should return only those with matching id when id option specified", function(){
            var results = select(data, { id: 2 });
            var expectation = [
                { id: 2, playTime: 2000, auto: true  },
                { id: 2, playTime: 2000, auto: true  }
            ];
            expect(results).deep.eql(expectation);
        });

        it("should return only results with auto: false when {auto: false} option specified", function(){
            var results = select(data, { auto: true });
            var expectation = [
                { id: 7, playTime: 1500, auto: true  },
                { id: 1, playTime:  100, auto: true  },
                { id: 2, playTime: 2000, auto: true  },
                { id: 2, playTime: 2000, auto: true  }
            ];
            expect(results).deep.eql(expectation);
        });

        it("should return only results with auto: false when {auto: false} option specified", function(){
            var results = select(data, { auto: false });
            var expectation = [
                { id: 8, playTime:  500, auto: false },
                { id: 7, playTime: 1000, auto: false },
                { id: 7, playTime: 2000, auto: false },
            ];
            expect(results).deep.eql(expectation);
        });

        it("should combine options id and minPlayTime", function(){
            var results = select(data, { id: 7, minPlayTime: 1500 });
            var expectation = [
                { id: 7, playTime: 1500, auto: true  },
                { id: 7, playTime: 2000, auto: false },
            ];
            expect(results).deep.eql(expectation);
        });

        it("should combine options id, minPlayTime, and auto", function(){
            var results = select(data, { id: 7, minPlayTime: 1500, auto: true });
            var expectation = [
                { id: 7, playTime: 1500, auto: true  }
            ];
            expect(results).deep.eql(expectation);
        });

        it("should respect minPlayTime", function(){
            var results = select(data, { minPlayTime: 4000 });
            expect(results).deep.eql([]);
        });

        it("should perform merges before any filtering", function(){
            var results = select(data, { merge: true, minPlayTime: 4000 });
            var expectation = [
                { id: 7, playTime: 4500, auto: false },
                { id: 2, playTime: 4000, auto: true  }
            ];
            expect(results).deep.eql(expectation);
        });
    });
});