#Aggregate Select
A javascript library for filtering track data collections.

##Installation
`npm install`

##Usage
There are three tasks defined in `package.json`.

 - `npm run test` will execute mocha tests in the test directory.
 - `npm run lint` will jshint test and src directories.
 - `npm run cli <filename> '<json options>'` allows you to test the library on sample data.

This lib can also be included via node `require` or made into an npm package.

##Design